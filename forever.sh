#!/bin/bash
# A simple script to control running this NodeJS server!

FILE="sherlock.js"
NAME="sherlock.proxy"
TIMESTAMP=$(date +"%s")

sudo true

BASE=`readlink -f "$0"`
BASE="`dirname "$BASE"`/"
pushd "$BASE"

sudo forever stop $FILE
echo $FILE" stopped"
mkdir ~/.forever/logs -p
mv ~/.forever/forever-$NAME.log ~/.forever/logs/$NAME.$TIMESTAMP.log
mkdir .forever/$TIMESTAMP -p
mv .forever/*.log .forever/$TIMESTAMP/
sudo forever start -l forever-$NAME.log -o .forever/out.log -e .forever/error.log $FILE
echo $FILE" started"

popd
