var http = require('http'),
	httpProxy = require('http-proxy'),
	router = require('./hosts');

httpProxy.createServer(
	function(req, res, proxy) {
		var destination = { "host" : "127.0.0.1", "port" : 82 };
		
		for(var route in router)
			if (route == req.headers.host)
				destination = router[route];
		
		proxy.proxyRequest(req, res, destination);
	}
).listen(80);

http.createServer(function (req, res) {
	res.writeHead(200, { 'Content-Type': 'application/json' });
	res.write(JSON.stringify(req.headers, true, 2));
	res.end();
}).listen(82);
