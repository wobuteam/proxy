# Wobu Proxy
## A simple proxy service

Quite literally, takes requests and send them to specific hosts & ports based on their hostname.

Nothing special at all.

Uses [http-proxy][httpproxy] and [forever][].

Forked from my old project! :)

~ [@jdrydn](http://jdrydn.com)

[sherlock]: https://github.com/jdrydn/sherlock
[forever]: https://github.com/nodejitsu/forever
[httpproxy]: https://github.com/nodejitsu/node-http-proxy
